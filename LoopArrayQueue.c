/*
 * LoopArrayQueue.c
 *
 *  Created on: 2017年4月17日
 *      Author: lilo
 */
#include <stdio.h>
#include <malloc.h>

#define MAXSIZE 30
#define ERROR 0
#define OK 1

typedef int Status;
typedef int Elemtype;
typedef struct SQueue{
	Elemtype data[MAXSIZE];
	int front;  /*  头  指  针   */
	int rear;  /*   尾 指 针   ，空 一 个 位 置 来 判 断 是 空 还 是 满 */
}SQueue;

Status outQueue(SQueue *q ,Elemtype *e);
Status inQueue(SQueue *q , Elemtype e);
int getLengthQueue(SQueue *q);
Status initQueue(SQueue * q);
Status showLoopArrayQueue(SQueue *q);

/*
int main(){
	SQueue *q = malloc(sizeof(SQueue));
	initQueue(q);
	inQueue(q,1);
	inQueue(q,2);
	showLoopArrayQueue(q);
	Elemtype e;
	outQueue(q , &e);
	showLoopArrayQueue(q);
	return 0;
}
*/

Status showLoopArrayQueue(SQueue *q){
	if(q->rear == q->front){
		printf("array queue is zero ,no data to out\n");
		return ERROR;
	}
	int len = getLengthQueue(q);
	int i = 0;
	while(i < len){
		printf("%d->",q->data[(q->front+i)%MAXSIZE]);
		i++;
	}
	printf("\n");
	return OK;
}

Status initQueue(SQueue * q){
	q->front = 0;
	q->rear = 0;
	return OK;
}

int getLengthQueue(SQueue *q){
	return (q->rear - q->front + MAXSIZE )%MAXSIZE;
}

Status inQueue(SQueue *q , Elemtype e){
	if( (q->rear+1)%MAXSIZE == q->front){
		printf("array queue is full\n");
		return ERROR;
	}
	q->data[q->rear] = e;
	q->rear = (q->rear+1) % MAXSIZE;

	return OK;
}

Status outQueue(SQueue *q ,Elemtype *e){
	if(q->rear == q->front){
		printf("array queue is zero ,no data to out\n");
		return ERROR;
	}
	*e = q->data[q->front];
	q->front = (q->front+1)%MAXSIZE;
	return OK;
}



