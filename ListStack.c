/*
 * ListStack.c
 *
 *  Created on: 2017��4��17��
 *      Author: lilo
 */
#include <stdio.h>
#include <malloc.h>

#define MAXSIZE 30
#define ERROR 1
#define OK 0

typedef int Elemtype;
typedef int Status;
typedef struct StackNode{
	Elemtype data;
	struct StackNode *next;
}StackNode,*LinkStackP;

typedef struct LinkStack{
	LinkStackP top;
	int count;
}LinkStack;

Status initListStack(LinkStack *S);
Status PushStack(LinkStack *S,Elemtype e);
Status showListStack(LinkStack *S);
Status PopStack(LinkStack *S ,Elemtype *e);

/*int main(){
	LinkStack *S = malloc(sizeof(LinkStack));
	initListStack(S);
	PushStack(S,1);
	PushStack(S,2);
	PushStack(S,3);
	showListStack(S);
	Elemtype e;
	PopStack(S,&e);
	printf("pop e: %d\n",e);
	showListStack(S);

	return 0;
}*/

Status showListStack(LinkStack *S){
	if(S->top == NULL || S->count == 0){
		printf("zero stack\n");
		return ERROR;
	}
	LinkStackP  p = S->top;
	int len = S->count;
	while(len >=  1){
		printf("%d->",p->data);
		p = p->next;
		len--;
	}
	printf("\n");
	return OK;
}

Status initListStack(LinkStack *S){
	S->count = 0;
	S->top = NULL;
	return OK;
}

Status PushStack(LinkStack *S,Elemtype e){
	LinkStackP p = malloc(sizeof(StackNode));
	p->data = e;
	p->next = S->top;
	S->top = p;
	S->count++;
	return OK;
}

Status PopStack(LinkStack *S ,Elemtype *e){
	if(S->top==NULL || S->count==0){
		printf("zero stack,no data to pop\n");
		return ERROR;
	}
	LinkStackP p = S->top;
	*e = p->data;
	S->top = p->next;
	S->count--;
	free(p);
	return OK;
}


