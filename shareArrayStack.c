/*
 * shareArrayStack.c
 *
 *  Created on: 2017年4月17日
 *      Author: lilo
 */
#include <stdio.h>
#include <malloc.h>

#define MAXSIZE 30
#define ERROR 1
#define OK 0

typedef int Elemtype;
typedef int Status;
typedef struct DoubleStack{
	Elemtype data[MAXSIZE];
	int top1;
	int top2;
}DoubleStack;
typedef DoubleStack *PStack;

Status initDoubleStack(PStack s);
Status PushElem(PStack s,Elemtype e,int numberStack);
Status PopElem(PStack s,Elemtype *e,int numberStack);
Status showDubleStack(PStack s);
/**
 * 优缺点就是可以当一个进一个出保持一个平衡
 * */
/*int main(){
	PStack S = malloc(sizeof(DoubleStack));
	Elemtype e;
	initDoubleStack(S);
	PushElem(S , 1 , 1);
	PushElem(S , 2 , 1);
	PushElem(S , 3 , 1);
	showDubleStack(S);
	PushElem(S , 3 , 2);
	PushElem(S , 4 , 2);
	PushElem(S , 5 , 2);
	showDubleStack(S);
	PopElem(S , &e , 2);
	printf("pop a element from 2STack %d\n",e);
	showDubleStack(S);
	return 0;
}*/

Status showDubleStack(PStack s){
	int k = 0;
	if(s->top1 != -1){
		printf("Stack1: ");
		while(k <= s->top1){
			printf("%d->",s->data[k]);
			k++;
		}
	}else{
		printf("Stack1 is zero");
	}
	printf("\n");
	k = MAXSIZE-1;
	if(s->top2 != MAXSIZE){
		printf("Stack2: ");
		while(k>=s->top2){
			printf("%d->",s->data[k]);
			k--;
		}
	}else{
		printf("Stack2 is zero");
	}
	printf("\n");
	return OK;
}

Status PopElem(PStack s,Elemtype *e,int numberStack){
	if( numberStack==1 && s->top1!=-1){
		*e = s->data[s->top1--];
	}else if(numberStack==1){
		printf("Stack1 is zero\n");
		return ERROR;
	}
	if(numberStack==2 && s->top2!=MAXSIZE){
		*e = s->data[s->top2++];
	}else if(numberStack==2){
		printf("Stack2 is zero\n");
		return ERROR;
	}
	return OK;
}

/**
 * 插入元素
 * numberStack 表示栈的id
 * */
Status PushElem(PStack s,Elemtype e,int numberStack){
	if(s->top1+1 == s->top2){
		printf("Stack is full , never push\n");
		return ERROR;
	}
	if(numberStack == 1){
		s->data[++s->top1] = e;
	}else if(numberStack == 2){
		s->data[--s->top2] = e;
	}
	return OK;
}


Status initDoubleStack(PStack s){
	s->top1 = -1;
	s->top2 = MAXSIZE ;
	return OK;
}


