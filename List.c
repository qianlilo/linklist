/*
 * List.c
 *
 *  Created on: 2017年4月17日
 *      Author: lilo
 */
#include <stdio.h>
#include <malloc.h>

#define MAXSIZE 30
#define ERROR 0
#define OK 1

typedef int Status;
typedef int Elemtype;
typedef struct Node{
	Elemtype data;
	struct Node *next;
}Node;
typedef struct Node *LinkList;

/**
 * 链表 ，主要在于 LinkList是头结点，没有内容只有指向第一个结点的地址
 * */
Status addElem(LinkList L,Elemtype e);
Status delete(LinkList L ,int i,Elemtype *e);
Status insert(LinkList L,int i,Elemtype e);
Status getElem(LinkList L , int i, Elemtype *e);
Status showALL(LinkList L);
Status init(LinkList L);
Status clear(LinkList L);
Status getLen(LinkList L);

/*int main(){
	LinkList L = malloc(sizeof(Node));
	init(L);
	addElem(L , 1);
	addElem(L , 2);
	addElem(L , 3);
	showALL(L);
	printf("length = %d\n",getLen(L));
	clear(L);
	showALL(L);
	printf("length = %d\n",getLen(L));
	insert(L,1,8);
	showALL(L);
	printf("length = %d\n",getLen(L));
	return OK;
}*/

/**getlen*/
Status getLen(LinkList L){
	LinkList p;
	p = L ;
	if(!p->next){
		printf("len is zero\n");
		return 0;
	}
	int len = 0;
	while(p->next){
		p = p->next;
		len++;
	}
	return len;
}

/**clear*/
Status clear(LinkList L){
	LinkList p , s;
	p = L->next;
	while(p){
		s = p->next;
		free(p);
		p = s;
	}
	L->next = NULL;
	return OK;
}

/**初始化*/
Status init(LinkList L){
	L->next = NULL;
	return OK;
}
/**show all*/
Status showALL(LinkList L){
	LinkList p ;
	p = L->next;
	if(p==NULL){
		printf("clear list\n");
		return ERROR;
	}
	while(p->next){
		printf("%d->",p->data);
		p = p->next;
	}
	printf("%d->\n",p->data);
	return OK;
}

/**delete*/
Status delete(LinkList L ,int i,Elemtype *e){
	LinkList p ,s;
	p = L;
	int j = 1;
	while((p->next) && j<i){
		p = p->next;
		j++;
	}
	if(!(p->next)||j>i){
		return ERROR;
	}
	s = p->next;
	p->next = s->next;
	/**在释放前先获取数据保存下来*/
	*e = s->data;
	free(s);
	return OK;
}
/**insert*/
Status insert(LinkList L,int i,Elemtype e){
	LinkList p , s;
	p = L;
	int j = 1;
	while(p && j<i){
		p = p->next;
		j++;
	}
	if(!p || j>i){
		return ERROR;
	}
	s = malloc(sizeof(Node));
	s->data = e;
	s->next = p->next;
	p->next = s;
	return OK;
}

/**get就是拿到值但是不删除*/
Status getElem(LinkList L , int i, Elemtype *e){
	LinkList p;
	p = L->next;
	int j = 1;
	while(p && j<i ){
		p = p->next;
		j++;
	}
	if(!p || j>i){
		return ERROR;
	}
	*e = p->data;
	return OK;
}
/**直接加到末尾*/
Status addElem(LinkList L,Elemtype e){
	LinkList p ,s;
	int len = 1;
	p = L;
	while(p->next){
		len++;
		p = p->next;
		if(len > MAXSIZE){
			return ERROR;
		}
	}
	s = malloc(sizeof(Node));
	s->data = e;
	s->next = NULL;
	p->next = s;
	return OK;
}


