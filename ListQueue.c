/*
 * ListQueue.c
 *
 *  Created on: 2017��4��17��
 *      Author: lilo
 */

#include <stdio.h>
#include <malloc.h>

#define ERROR 0
#define OK 1

typedef int Status;
typedef int Elemtype;
typedef struct QNode{
	Elemtype data;
	struct QNode* next;
}QNode,*QNodeP;

typedef struct{
	QNodeP front ,rear;
}LinkQueue;

Status outListQueue(LinkQueue *Q ,Elemtype *e);
Status initListQueue(LinkQueue *Q);
Status inListQueue(LinkQueue *Q,Elemtype e);
Status showListQueue(LinkQueue *q);

int main(){
	LinkQueue *L = malloc(sizeof(LinkQueue));
	initListQueue(L);
	inListQueue(L , 1);
	inListQueue(L , 2);
	inListQueue(L , 3);
	showListQueue(L);
	Elemtype e;
	outListQueue(L,&e);
	showListQueue(L);

	return OK;
}

Status showListQueue(LinkQueue *q){
	QNodeP p = q->front->next;
	while(p != q->rear){
		printf("%d->",p->data);
		p = p->next;
	}
	printf("%d\n",p->data);
	return OK;
}

Status outListQueue(LinkQueue *Q ,Elemtype *e){
	if(Q->rear == Q->front){
		return ERROR;
	}
	QNodeP p = Q->front->next;
	*e = p->data;
	Q->front->next = p->next;
	if(Q->rear == p){
		Q->rear = Q->front;
	}
	free(p);
	return OK;
}

Status initListQueue(LinkQueue *Q){
	QNodeP p = malloc(sizeof(QNode));
	p->next = NULL;
	Q->front = p;
	Q->rear = p;
	return OK;
}

Status inListQueue(LinkQueue *Q,Elemtype e){
	QNodeP p = malloc(sizeof(QNode));
	p->data = e;
	p->next = NULL;
	Q->rear->next = p;
	Q->rear =  p;
	return OK;
}
