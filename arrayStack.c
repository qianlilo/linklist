/*
 * arrayStack.c
 *
 *  Created on: 2017年4月17日
 *      Author: lilo
 */
#include <stdio.h>
#include <malloc.h>

#define MAXSIZE 50
#define OK 1
#define ERROR 0

typedef int Status;
typedef int ElemType;
typedef struct{
	ElemType data[MAXSIZE];
	int top;
}ArrayStack;

Status Push(ArrayStack *s,ElemType e);
Status Pop(ArrayStack *s,ElemType *e);
Status initArrayStack(ArrayStack *s);
int getLength(ArrayStack *s);
Status isEmpty(ArrayStack *s);
Status showStack(ArrayStack *s);
Status clearStack(ArrayStack *s);
Status getTop(ArrayStack *s,ElemType *e);
/**
 * 用数组来实现栈的功能，主要要注意用top这一项来实现指针的位置
 *
 *
 * */
/*int main(){
	ArrayStack *s = malloc(sizeof(ArrayStack));
	ElemType e;
	initArrayStack(s);
	Push(s,1);
	Push(s,3);
	Push(s,4);
	Pop(s,&e);
	getTop(s,&e);
	printf("%d\n",e);
	showStack(s);
	clearStack(s);
	showStack(s);
	return OK;
}*/



/**插入元素*/
Status Push(ArrayStack *s,ElemType e){
	if(s->top == MAXSIZE-1){
		return ERROR;
	}
	s->top++;
	s->data[s->top] = e;
	return OK;
}
/**删除操作*/
Status Pop(ArrayStack *s,ElemType *e){
	if(s->top == -1){
		return ERROR;
	}
	*e = s->data[s->top];
	s->top--;
	return OK;
}
/**初始化*/
Status initArrayStack(ArrayStack *s){
	/**-1 说明是空*/
	s->top = -1;
	return OK;
}

/**返回栈长度*/
int getLength(ArrayStack *s){
	return (s->top)+1;
}

/**若栈为空，返回true*/
Status isEmpty(ArrayStack *s){
	if(s->top == -1)
		return OK;
	return ERROR;
}

/**打印所有内容*/
Status showStack(ArrayStack *s){
	if(s->top == -1 || s->top>=MAXSIZE){
		return ERROR;
	}
	while(s->top != -1){
		printf("%d\t",s->data[s->top]);
		s->top--;
	}
	printf("\n");
	return OK;
}

/**清空栈*/
Status clearStack(ArrayStack *s){
	if(s->top == -1 || s->top>=MAXSIZE){
		printf("this stack is already null\n");
		return ERROR;
	}
	while(s->top != -1){
		s->data[s->top] = 0;
		s->top--;
	}
	return OK;
}
/**返回s的栈顶元素*/
Status getTop(ArrayStack *s,ElemType *e){
	if(s->top==-1 || s->top>= MAXSIZE-1){
		return ERROR;
	}
	*e = s->data[s->top];
	return OK;
}

