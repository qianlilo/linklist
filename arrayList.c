/*
 * main.c
 *
 *  Created on: 2017年4月16日
 *      Author: lilo
 */
#include <stdio.h>
#include <malloc.h>
/**
 * 线性顺序存储链表，用数组存储
 *
 * */
#define OK 1
#define ERROR 0
#define FALSE 0
#define MAXSIZE 30

typedef int ElemType;
typedef int Status;

/**
 * @parms lenth 线性表当前长度
 * */
typedef struct{
	ElemType data[MAXSIZE];
	int length;
}SQList;

Status getElement(SQList *L , int i, ElemType *e);
Status ListInsert(SQList *L,int i,ElemType e);
Status addToEnd(SQList *L,ElemType e);
Status ListDelete(SQList *L,int i,ElemType *e);
void initList(SQList *L);
void showList(SQList *L);

/**测试主代码*/
//int main(int argc,char **argv){
//	struct SQList *L = malloc(sizeof(SQList));
//	int len = 10;
//	int i;
//	initList(L);
//	for(i=0;i<len;i++){
//		ElemType e = i;
//		addToEnd(L,e);
//	}
//	showList(L);
//	ElemType e1;
//	getElement(L ,5, &e1);
//	printf("\n%d\n",e1);
//	showList(L);
//	ListDelete(L,6,&e1);
//	printf("\n%d\n",e1);
//	showList(L);
//	return 0;
//}

/**
 * 获得第i个数据存到e
 * */
Status getElement(SQList *L , int i, ElemType *e){
	if(L->length == 0 || i<1 || i>L->length){
		return ERROR;
	}
	*e = L->data[i-1];
	return OK;
}
/**
 * 插入数据到0-length
 * */
Status ListInsert(SQList *L,int i,ElemType e){
	int k;
	if(L->length == MAXSIZE){
		return ERROR;
	}
	if(i<1 || i>L->length){
		return ERROR;
	}
	/**判断不是插在末尾*/
	if(i<= L->length){
		for(k=L->length-1; k>=i-1;k--){
			L->data[k+1] = L->data[k];
		}
	}
	L->data[i-1] = e;
	L->length++;
	return OK;
}

/**加到末尾*/
Status addToEnd(SQList *L,ElemType e){
	if(L->length == MAXSIZE){
		return ERROR;
	}
	L->data[L->length] = e;
	L->length++;
	return OK;
}

/**删除操作*/
Status ListDelete(SQList *L,int i,ElemType *e){
	int k;
	if(L->length==0){
		return ERROR;
	}
	if(i<1 || i>L->length){
		return ERROR;
	}
	if(i<L->length){
		*e = L->data[i-1];
		for(k=i-1;k<L->length;k++){
			L->data[k] = L->data[k+1];
		}
	}
	L->length--;
	return OK;
}
/**初始化链表*/
void initList(SQList *L){
	L->length = 0;
}
/**打印所有成员*/
void showList(SQList *L){
	int len=L->length;
	int i;
	for(i=0;i<len;i++){
		printf("%d->",L->data[i]);
	}
}

